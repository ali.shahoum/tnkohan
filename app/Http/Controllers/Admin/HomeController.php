<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Products_price;
use App\Models\TimeSort;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function getIndex(){
        return view('admin.index');

    }
    public function getShow(){
        $product=Product::query()->get();
        return view('admin.product.show')->
        with('product',$product)
            ;

    }
    public function showTime($id){
        $pro=Product::find($id);
        $product=Product::query()->get();
        return view('admin.product.show')->
        with('pro',$pro)->
        with('product',$product)
            ;

    }
    public function showPrice($id){
        $pro=Product::find($id);
        $product=Product::query()->get();
        return view('admin.product.show_price')->
        with('pro',$pro)->
        with('product',$product)
            ;
    }
    public function product(){
        $time=TimeSort::query()->get();
        $product=Product::query()->get();
        $tp=Products_price::query()->get();
        return view('admin.product.index')->
            with('product',$product)->
            with('tp',$tp)->
            with('time',$time);

    }

    public function store(Request $request) {
        $product = new Product;
        $product->name = $request->name;
        $product->save();
        return response()->json(['ایتم با موفقیت ایجاد شد']);

    }
    public function storeCat(Request $request) {
//        Log::info($request);

            $timeSort = new TimeSort();
            $timeSort->cat = $request->cat;
            $timeSort->save();
            return response()->json(['ایتم با موفقیت ایجاد شد']);
        }
    public function price(Request $request)
    {
//        $req = Products_price::query()->
//        where('product_id', $request['product_id'])->where('time_sort', $request['time_sort'])->first();
//        Log::info($req);
//        if ($req == true) {
//            return Redirect::back()->with('warning', 'تکراری.');
//        } else {
//            $product = new Products_price;
//            $product->product_id = $request->product_id;
//            $product->time_sort = $request->time_sort;
//            $product->price = $request->price;
//            $product->save();
//            return response()->json(['ایتم با موفقیت ایجاد شد']);
//
//        }
        $product=Product::find($request['id']);
        if ($request['time_sort']==1){
            $product['price1']=$request['price'];
        }elseif ($request['time_sort']==2){
            $product['price2']=$request['price'];
        }elseif ($request['time_sort']==3){
            $product['price3']=$request['price'];
        }else {
        $product['price4']=$request['price'];
        }
        $product->update();
        return response()->json(['ایتم با موفقیت ثبت شد']);


    }

}
