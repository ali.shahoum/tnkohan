<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected $table='products';

    protected $fillable=['name','price1','price2','price3','price4'];

    public function product()
    {
        return $this->belongsTo('App\Models\Products_price', 'id', 'product_id');
    }

}