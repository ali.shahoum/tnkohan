<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeSort extends Model
{
    public $timestamps = false;
    protected $table='time_sort';

    protected $fillable=['cat'];}
