<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products_price extends Model
{
    public $timestamps = false;
    protected $table='Products_price';

    protected $fillable=['price','product_id','time_sort'];

    public function pro()
    {
        return $this->hasMany('App\Models\Product', 'id');
    }


}