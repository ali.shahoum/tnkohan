<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','family','mobile', 'email', 'password','image',
        'status','admin'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function ctr()
    {
        return $this->belongsTo('App\Models\Role', 'id');
    }


    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function deleteRole($role)
    {
        return $this->roles()->detach($role);
    }
    public function hasPermission($access)
    {
        $accessCount = count(explode('.', $access));

        foreach ($this->roles as $userRole) {

            $allPermissions = unserialize($userRole->permission);
            if ($allPermissions['fullAccess'] == 1) {
                return true;
            }
            if ($accessCount == 1) {
                if (array_key_exists($access, $allPermissions)) {
                    return true;
                }
            }
            if (is_array($allPermissions)) {

                if (array_key_exists($access, array_dot($allPermissions))) {
                    return true;
                }

                return false;
            }

            return false;
        }

        return false;
    }
    public function st()
    {
        return $this->belongsTo('App\Models\State', 'state','code');
    }
    public function ct()
    {
        return $this->belongsTo('App\Models\City', 'city','id');
    }
}
