<?php


use App\Models\Catalog;
use App\Models\Services;
use App\Models\Setting;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

$setting = [];
if (Schema::hasTable('setting'))
    $setting = Setting::first();


View::composer('layout.admin.blocks.nav_top', function ($view) {
    $user= Auth::user();
    $view->with('user', $user);
});
View::composer('layout.admin.blocks.nav_right', function ($view) {
    $user= Auth::user();
    $view->with('user', $user);
});
View::composer('layout.site.blocks.footer', function ($view) {
    $ser=Services::where('status','services')->orderBy('id','asc')->take(3)->get();
    $view->with('ser', $ser);
});
View::composer('layout.site.blocks.header', function ($view) {
    $catalog=Catalog::get();
    $view->with('catalog', $catalog);
});

View::share([
    'setting' => $setting,

]);
