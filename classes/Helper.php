<?php

namespace Classes;
use App\Models\State;
use mPDF;
use Carbon\Carbon;
require_once "pdf/mpdf.php";

class Helper
{
    public static function pdfFile($html)
    {
        set_time_limit(300);
        $mpdf = new mPDF('ar');
        $mpdf->SetDirectionality('rtl');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        return $mpdf->Output();
    }

    public static function getDatePersian($date, $type)
    {

        $miladi = Carbon::parse($date);
        $timestamp = mktime($miladi->hour, $miladi->minute, $miladi->second, $miladi->month, $miladi->day, $miladi->year);
        $resualt = jdate($type, $timestamp);
        return $resualt;

    }
    public static function mine($id){
         $state = State::find($id);
         $mine =[
             0=>[
                 'id'=>$state->id,
                 'pname'=>$state->pname,
             ]
         ];
         $states = State::get();
         foreach ($states as $row){
             $mine [] =[
                 'id'=>$row->id,
                 'pname'=>$row->pname,
             ];
         }
         return $mine;
    }





}