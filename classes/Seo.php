<?php
namespace Classes;


class Seo
{
    public static function Seo($var, $lower = true, $punkt = true)
    {
        $var = str_replace(".php", "", $var);
        $var = trim(strip_tags($var));
        $var = preg_replace("/\s+/ms", "_", $var);
        if (strlen($var) > 70) {
            $var = substr($var, 0, 170);
            if (($temp_max = strrpos($var, '-'))) $var = substr($var, 0, $temp_max);
        }
        $search = array('+', '/', '@', '#', '"', '=', '\\', '.', '(', ')','?','؟');
        $replace = array('');

        return str_replace($search, $replace, $var);

    }

}