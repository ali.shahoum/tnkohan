<?php

namespace Classes;

use App\Models\Setting;
use Illuminate\Support\Facades\File;

class UploadImg
{
    public function uploadPic($file, $path, $resize = true, $extension = null, $base64 = false)
    {
        $pathMain = $path . '/main/';
        if ($resize) {
            $pathBig = $path . '/big/';
            $pathMedium = $path . '/medium/';
        }

        if (is_null($extension)) {
            $extension = $file->getClientOriginalExtension();
        }
        $ext = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'svg'];
        if (in_array($extension, $ext)) {
            if (!File::isDirectory($path)) {
                File::makeDirectory($path);
            }
            if (!File::isDirectory($pathMain)) {
                File::makeDirectory($pathMain);
            }
            if ($resize) {
                if (!File::isDirectory($pathBig)) {
                    File::makeDirectory($pathBig);
                }
                if (!File::isDirectory($pathMedium)) {
                    File::makeDirectory($pathMedium);
                }
            }

            $fileName = md5(microtime()) . ".$extension";
            if (!$base64) {
                $file->move($pathMain, $fileName);
            } else {
                list($type, $data) = explode(';', $file);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $loc = $path . '/main/' . $fileName;
                file_put_contents($loc, $data);
            }

            if ($resize) {
                $kaboom = explode(".", $fileName);
                $fileExt = end($kaboom);
                Resizer::resizePic($pathMain . $fileName, $pathMedium . $fileName, 400, 400, $fileExt);
                Resizer::resizePic($pathMain . $fileName, $pathBig . $fileName, 1600, 1600, $fileExt, True);
            }
            return $fileName;
        } else {
            return false;
        }
    }

    public function waterMark($fileName, $path)
    {
        $setting = Setting::find(1);
        $mark = new Watermark();

        $mark->thumbnail($path . 'big/' . $fileName);
        $mark->insert_watermark('', 'assets/uploads/setting/main/' . $setting->logo_water_mark);
        $mark->save($path . 'big/' . $fileName);

        $mark->thumbnail($path . 'medium/' . $fileName);
        $mark->insert_watermark('', 'assets/uploads/setting/main/' . $setting->logo_water_mark);
        $mark->save($path . 'medium/' . $fileName);

        return true;
    }
}