<?php
return [
    'admin' => 'admin',
    'panel' => 'panel',
    'permisions' => [

        'user' => array(
            'title' => 'مدیران',
            'access' => array(
                'index' => 'مشاهده',
                'add' => 'اضافه',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'group' => 'مشاهده دسترسی',
                'groupAdd' => 'اضافه دسترسی',
                'groupEdit' => 'ویرایش دسترسی',
                'groupDelete' => 'حذف دسترسی',
            )
        ),
        'setting' => array(
            'title' => 'تنظیمات',
            'access' => array(
                'edit' => 'ویرایش',
            )
        ),
        //in edit ke injast bayad ba url va route yeki bashe ke yani be in edit permishion ro mide

        'article' => array(
            'title' => 'مقالات',
            'access' => array(
                'list' => 'لیست',
                'add' => 'افزودن',
                'delete' => 'حذف',
                'edit' => 'ویرایش',
                'sort' => 'ترتیب',
                'logout' => 'خروج',

            )
        ),
        'gallery' => array(
            'title' => 'گالری',
            'access' => array(
                'list' => 'لیست',
                'add' => 'افزودن',
                'delete' => 'حذف',
                'logout' => 'خروج',

            )
        ),
        'ticket' => array(
            'title' => 'تیکت',
            'access' => array(
                'list' => 'لیست',
                'delete' => 'حذف',

            )
        ),
        'siteMap' => array(
            'title' => 'سایت مپ',
            'access' => array(
                'list' => 'لیست',
                'delete' => 'حذف',

            )
        ),
        'services' => array(
            'title' => 'خدمات',
            'access' => array(
                'loges' => 'لیست غرفه',
                'decor' => ' لیست دکور',
                'ads' => ' لیست تبلیغات',
                'add_loges' => 'افزودن غرفه',
                'add_decor' => 'افزودن دکوداسیون',
                'add_ads' => 'افزودن تبلیغات',
                'delete' => 'حذف',
                'edit' => 'ویرایش',
                'more_image' => 'تصاویر بیشتر',
                'logout' => 'خروج',

            )
        ),
        'pod' => array(
            'title' => 'پادکست',
            'access' => array(
                'list' => 'لیست',
                'add' => 'افزودن',
                'delete' => 'حذف',
                'edit' => 'ویرایش',
                'sort' => 'ترتیب',
                'logout' => 'خروج',

            )
        ),

        'product' => array(
            'title' => 'اسلایدر',
            'access' => array(
                'list' => 'لیست',
                'add' => 'افزودن',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'ترتیب',
                'logout' => 'خروج',
            )

        ) ,
        'manage' => array(
            'title' => 'سطح دسترسی',
            'access' => array(
                'list' => 'لیست',
                'add' => 'افزودن',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
                'sort' => 'ترتیب',
                'logout' => 'خروج',
            )

        ) ,
        'uploader' => array(
            'title' => 'اپلودر',
            'access' => array(
                'list' => 'مشاهده',
                'add' => 'افزودن',
                'edit' => 'ویرایش',
                'delete' => 'حذف',
            )

        ) ,




    ]
];