<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTable2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'ali')->exists()) {
            $input = [
                'name' => 'مدیر',
                'family' => 'سایت',
                'email' => 'ali',
                'password' => bcrypt('111111'),
                'remember_token' => str_random(10),
            ];
            $data = User::create($input);

            $input = [
                'name' => 'مدیر کل',
                'permission' => 'a:2:{s:10:"fullAccess";i:1;s:4:"user";a:3:{s:3:"add";i:1;s:4:"edit";i:1;s:6:"delete";i:1;}}',
            ];
            $role = Role::create($input);

            $data->assignRole($role);
        }
    }
}
