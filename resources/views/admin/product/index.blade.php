@extends('layout.admin.master')
@section('content')
    @include('layout.admin.blocks.message')

    <div class="right_col" role="main">

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>افزودن محصول
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="productForm" >
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">نام محصول</label>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="نام محصول">
                                </div>

                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>افزودن زمان بندی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="timeForm" >
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">انتخاب</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="cat" class="form-control" id="cat">
                                        <option>انتخاب زمان</option>
                                        <option value="1">صبح</option>
                                        <option value="2">ظهر</option>
                                        <option value="3">عصر</option>
                                        <option value="4">شب</option>
                                        {{--@foreach($time as $row2)--}}
                                        {{--<option value="{{$row2->id}}" > @if($row->cat==1)۱-۶ @elseif($row->cat==2) ۶-۱۲ @elseif($row->cat==3) ۱۲-۱۸ @else ۱۸-۲۴ @endif </option>--}}
                                        {{--@endforeach--}}
                                    </select>
                                </div>
                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> افزودن  قیمت محصول در هر زمان بندی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="priceForm" >
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <select name="product_id" class="form-control" id="product_id">
                                        <option>انتخاب محصول</option>
                                        @foreach($product as $row1)
                                            <option value="{{$row1->id}}" > {{$row1->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <select name="time_sort" class="form-control" id="time_sort">
                                        <option value="">انتخاب زمان</option>
                                        @foreach($time as $row)
                                            <option value="{{$row->id}}" > @if($row->cat==4)شب @elseif($row->cat==1)صبح  @elseif($row->cat==2) ظهر @else عصر @endif </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" name="price" id="price" class="form-control" placeholder="قیمت محصول">
                                </div>

                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>جدول محصولات
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <center>
                                    <input type="checkbox" name="select_all" value="1" id="select_all">
                                </center>
                                {{--<div class="icheckbox_flat-green" style="position: relative;"><input id="check-all" class="flat" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>--}}
                            </th>
                            <th>نام محصول</th>
                            <th>قیمت صبح</th>
                            <th>قیمت ظهر</th>
                            <th>قیمت عصر</th>
                            <th>قیمت شب</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $key=>$row)
                                <tr class="even pointer">

                            <td class="a-center ">
                                <center>
                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                           type="checkbox"
                                           value="{{$row->id}}"/>

                                </center>
                                {{--<div class="icheckbox_flat-green" style="position: relative;"><input class="flat" name="table_records" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>--}}
                            </td>
                            <td class=" ">{{$row->name}}</td>
                            <td class=" ">{{$row->price1}}</td>
                            <td class=" ">{{$row->price2}}</td>
                            <td class=" ">{{$row->price3}}</td>
                            <td class=" ">{{$row->price4}}</td>

                                @endforeach
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script type="text/javascript">

        $('#productForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            name = $('#name').val();

            $.ajax({
                url: "product/create",
                type:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    name:name,
                },
                success:function(response){
                    $("#productForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.info(response);
                    },

            });
        });

</script>

        <script>

        $('#timeForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            cat = $('#cat').val();

            $.ajax({
                url: "product/time",
                type:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    cat:cat,
                },
                success:function(response){
                    $("#timeForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.success(response);
                    },
                error: function (response)
                {
                    toastr.warning('این زمانبندی وجود دارد');
                }
            });
        });
    </script>
    <script type="text/javascript">

        $('#priceForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            id = $('#product_id').val();
            time_sort = $('#time_sort').val();
            price = $('#price').val();

            $.ajax({
                url: "product/price",
                type:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    id:id,
                    time_sort:time_sort,
                    price:price,
                },
                success:function(response){
                    $("#priceForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.info(response);
                },

            });
        });

    </script>
            <script type="text/javascript">
            $(document).ready(function () {
                $("#select_all").on('click', function () {
                    $.each($("input"), function (index, value) {
                        if (value.type == 'checkbox') {
                            value.checked = $("#select_all")[0].checked;
                        }
                    });
                });
            });
    </script>

    @endsection
