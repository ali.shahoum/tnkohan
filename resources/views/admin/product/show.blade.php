@extends('layout.admin.master')
@section('content')
    <div class="right_col" role="main">

        <div class="col-md-10 col-sm-10 col-xs-10">
            <div class="x_panel">
                <div class="x_title">
                    <h2>لیست محصولات
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers" >
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border-bottom: 4px solid #e6e9ed;">
                                    @foreach($product as $row)
                                  <a href="{{ url('admin/show/showTime/'.$row->id) }}"  class="btn btn-success">{{$row->name}}</a>
                                        @endforeach
                                </div><br>
                            </div>
                    </div>
                    <br>
                    @if(@$pro['price1'] or @$pro['price2']or@$pro['price3']or@$pro['price4'])
                        <h5 >لیست زمانهای تعین شده برای  محصول{{$pro->name}}
                        </h5>
                        <div class="clearfix"></div>

                        <div class="bs-example-popovers">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    @if(@$pro['price1'])<a href="{{ url('admin/show/showPrice/'.$pro->id) }}" class="btn btn-dark" style="margin-right: 30px">صبح</a>@endif
                                    @if(@$pro['price2'])<a href="{{ url('admin/show/showPrice/'.$pro->id) }}" class="btn btn-dark" style="margin-right: 30px">ظهر</a>@endif
                                    @if(@$pro['price3'])<a href="{{ url('admin/show/showPrice/'.$pro->id) }}" class="btn btn-dark" style="margin-right: 30px">عصر</a>@endif
                                    @if(@$pro['price4'])<a href="{{ url('admin/show/showPrice/'.$pro->id) }}" class="btn btn-dark" style="margin-right: 30px">شب</a>@endif
                                </div>
                            </div>
                        </div>
                    @endif

                    <!-- end pop-over -->

                </div>
            </div>
        </div>


    </div>


@endsection
