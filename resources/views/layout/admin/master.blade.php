<!DOCTYPE html>
<html lang="fa" dir="rtl">
@include('layout.admin.blocks.head')
<body class="nav-md">
<div class="container body">
    <div class="main_container">
    @include('layout.admin.blocks.nav_right')
    @include('layout.admin.blocks.nav_top')



    @yield('content')
        <!-- /page content -->

        <!-- footer content -->

        @include('layout.admin.blocks.nav_footer')
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
@include('layout.admin.blocks.scripts')
@include('layout.admin.blocks.message')

</body>
</html>
