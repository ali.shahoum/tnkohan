<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;


Route::get('/admin', 'Admin\HomeController@getIndex' );
Route::get('/admin/product', 'Admin\HomeController@product' );
Route::get('/admin/show', 'Admin\HomeController@getShow' );
Route::post('admin/product/create', 'Admin\HomeController@store');
Route::post('admin/product/time', 'Admin\HomeController@storeCat');
Route::post('admin/product/price', 'Admin\HomeController@price');
Route::get('admin/show/showTime/{id}', 'Admin\HomeController@showTime');
Route::get('admin/show/showPrice/{id}', 'Admin\HomeController@showPrice');
